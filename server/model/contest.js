const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);
const contestSchema = new mongoose.Schema({
    contest_id : String,
    contest_title : String,
    personnel_organization : String,
    multiplayer: String,
    contest_start : String,
    contest_end : String,
    description : String,
    registration_date : String,
    registration_id : String,
    update_date : String,
    update_id : String,
    record_list : [{
      hunter_id : String,
      hunter_name : String,
      record_m : String,
      record_s : String,
      record_ms : String,
      weapon : String,
      url : String,
      r_date : String,
      u_date : String
       }]
}, { collection : 'contest' });

// Create new contest document
contestSchema.statics.create = function (contestInfo) {
    const contestSchema = new this(contestInfo);
    return contestSchema.save();
  };

// Find All
contestSchema.statics.findAll = function () {
    return this.find({});
};

// contestSchema.statics.find = function (contest_id) {
//   if(contest_id)
//   return this.find({ "contest_id": contest_id });
// };

contestSchema.statics.findDetail = function (contest_id) {
  if(contest_id)
    return this.aggregate([
        { "$match": { "contest_id": contest_id }},
        { "$unwind": '$record_list' },
        { "$sort": {
          'record_list.record_m': 1,
          'record_list.record_s': 1,
          'record_list.record_ms': 1
        }}
      ]);
  };

contestSchema.statics.contestEnter = function (contest_id, hunter_id, hunter_name) {
  if(contest_id)
    return this.update(  
      { "contest_id": contest_id } ,
      { 
        "$push": { 
          "record_list": 
            {
              "$each":[{
                "hunter_id":hunter_id
              , "hunter_name":hunter_name
              , "record_m":99
              , "record_s":99
              , "record_ms":'999'
              , "weapon":''
              , "url":''
            }]
          }
        } 
      }
    );
  };

  contestSchema.statics.updateRecord = function (contest_id, hunter_id, hunter_name, record_m, record_s, record_ms, weapon, url) {
    if(contest_id)
      return this.findOneAndUpdate(
      { "contest_id": contest_id , "record_list": { "$elemMatch": { "hunter_id": hunter_id } }} ,
      { "$set":  { 
           "record_list.$.record_m":record_m
          , "record_list.$.record_s":record_s
          , "record_list.$.record_ms":record_ms
          , "record_list.$.weapon":weapon
          , "record_list.$.url":url
      } }
      ,{ multi: true }
      );
    };

module.exports = mongoose.model('contest', contestSchema);