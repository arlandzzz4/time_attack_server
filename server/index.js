const express = require("express");
const session = require('express-session');
const cors = require('cors');
const MongoStore = require('connect-mongodb-session')(session);
const app = express();
const logger = require('./env/logger')

// process.env.NODE_ENV = ( process.env.NODE_ENV && ( process.env.NODE_ENV ).trim().toLowerCase() == 'production' ) ? 'production' : 'development';

// let prop;
// console.log("process.env.NODE_ENV : ", process.env.NODE_ENV);
// if (process.env.NODE_ENV == 'production') {
//     prop = require("./env/real.json");
// } else if (process.env.NODE_ENV == 'development') {
//     prop = require("./env/local.json");
// }
const hostJson = require("./env/host.json");
const host = hostJson ? hostJson.host : "local";

const prop = require("./env/"+host+".json");

process.env.DBURL = prop.dbUrl;

const conn = require("./mongo/server.js");
 
logger.debug(prop.hostIp);
const corsOptions = {
    origin: 'http://'+prop.hostIp,// + prop.hostUrl, // 허락하고자 하는 요청 주소 http://localhost:8090
    optionsSuccessStatus: 200 ,
    credentials: true // true로 하면 설정한 내용을 response 헤더에 추가 해줍니다.
};
app.use(cors(corsOptions));

app.use(express.json());
app.use(express.urlencoded({ extended: true}));

app.use(session({
    cookieName: 'session',
    duration: 30 * 60 * 1000,
    activeDuration: 5 * 60 * 1000,
    secret: 'What The Fuck',
    resave: false,
    saveUninitialized: true,
    store: new MongoStore({ 
        mongooseConnection: conn.db
    })
}));

app.use(express.static("dist"));

//라우트
var root = require('./routes/root.js');
app.use('/', root);

var contest = require('./routes/contest.js');
app.use('/ct', contest);

var hunter = require('./routes/hunter.js');
app.use('/ht', hunter);

var common = require('./routes/common.js');
app.use('/cm', common);


// const https = require('https');
// const fs = require('fs');

// const options = {
//     key: fs.readFileSync('etc/fixtures/keys/agent2-key.pem'),
//     cert: fs.readFileSync('etc/fixtures/keys/agent2-cert.pem')
//   };
// https.createServer(options, app).listen(3000);
app.listen(3000, () => console.log("Listening on port 3000!"));
